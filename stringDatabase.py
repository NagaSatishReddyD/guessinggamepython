class StringDatabase:
    """
    StringDatabase class contains the disk I/O operations and provides required data to start the game
    @author: Naga Satish Reddy Dwarampudi
    """
    @staticmethod
    def get_words():
        """
        get_words method reads the words from the file and makes a list and returns
        :return: words_list, lists of words in the file
        """
        input_file = open("four_letters.txt", "r")
        words_list = []
        for line in input_file:
            line.strip()
            words_list.extend(line.split(' '))
        return words_list

    @staticmethod
    def get_scores_for_words():
        """
        contains the data letters frequencies.
        :return: dictionary of the letter frequencies used to calculate the score
        """
        return {'a': 8.17, 'b': 1.49, 'c': 2.78, 'd': 4.25, 'e': 12.70, 'f': 2.23, 'g': 2.02, 'h': 6.09, 'i': 6.97,
                'j': 0.15, 'k': 0.77, 'l': 4.03, 'm': 2.41, 'n': 6.75, 'o': 7.51, 'p': 1.93, 'q': 0.10, 'r': 5.99,
                's': 6.33, 't': 9.06, 'u': 2.76, 'v': 0.98, 'w': 2.36, 'x': 0.15, 'y': 1.97, 'z': 0.07}
