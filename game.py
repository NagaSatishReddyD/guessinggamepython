class Game:
    """
        Game class is a model for each game data. It contains the data
        @author: Naga Satish Reddy Dwarampudi
    """
    def __init__(self, game_number, word):
        """
        This method is initializes the required data when a new game object created
        :param game_number: count of word the user is to be played
        :param word: word which is picked randomly
        """
        self.game_number = game_number
        self.word = word
        self.status = 'Failed'
        self.bad_guesses = 0
        self.missed_letters = 0
        self.score = 0

    def set_status(self, status):
        """
        set_status sets the status attribute whether Gave Up or Successfully gussed
        :param status:
        """
        self.status = status

    def set_bad_guesses(self):
        """
        set_bad_guesses increments the bad guess count
        """
        self.bad_guesses += 1

    def set_missed_letters(self):
        """
        set_missed_letters incremens the missed letters from the guess
        """
        self.missed_letters += 1

    def set_score(self, score):
        """
        set_score updates the score of the game and round to the two decimal points
        :param score: score of the action by the player
        """
        self.score += score
        self.score = round(self.score, 2)
