import random
import stringDatabase
import game


class Guess:
    """
        guess.py is the file where the GuessingGame starts
        @author: Naga Satish Reddy Dwarampudi
    """

    def start_game(self, words_list, letter_frequencies):
        """
        start_game method loops the game to run continuously until the user quit the game
        and it also checks the user input of options and triggers the appropriate methods based
        on the user inputs
        :param words_list: list of words used in the game
        :param letter_frequencies: frequencies of letters used and is used to count score obtained by the player
        """
        game_count = 1
        word = words_list[random.randint(0, len(words_list))]
        current_guess = '----'
        print('** Guessing Game\n\n')
        is_game_running = True
        games_played_data = []
        game_object = game.Game(game_count, word)
        methods_name = {'g': self.guess_word, 't': self.tell_me, 'l': self.check_my_letter, 'q': 'quit_game'}
        while is_game_running:
            print('Current Word ' + game_object.word)
            print('Current Guess ' + current_guess)
            print("g guess, t = tell me, l for letter, and q to quit")
            letter_entered = input('Enter your option')
            option_elected = methods_name.get(letter_entered, 'Invalid Input')
            if option_elected == 'quit_game':
                game_exit = input('Are you really want to exit(Y/N)')
                if game_exit.casefold() == 'y':
                    is_game_running = False
            elif option_elected == self.check_my_letter:
                current_guess = self.check_my_letter(game_object, current_guess, letter_frequencies)
            elif option_elected == self.guess_word:
                current_guess = self.guess_word(game_object, current_guess, letter_frequencies)
            elif option_elected == self.tell_me:
                current_guess = self.tell_me(game_object, current_guess, letter_frequencies)

            if current_guess == game_object.word:
                print('New Word To Guess')
                games_played_data.append(game_object)
                words_list.remove(word)
                word = words_list[random.randint(0, len(words_list))]
                current_guess = '----'
                game_count += 1
                game_object = game.Game(game_count, word)

        self.show_games_result(games_played_data)

    def show_games_result(self, games_played_data):
        """
        show_game_result method prints the final result of the player that how many games has been played
        by the user and also prints the final score of all the games.

        :param games_played_data: all the games data till player played before quit
        """
        print(len(games_played_data))
        print('Game\tWord\tStatus\tBad Guess\tMissed Letters\tScore\n')
        print('----\t----\t------\t--------\t--------------\t-----')
        final_score = 0
        for game_played in games_played_data:
            final_score += game_played.score
            print(game_played.game_number, '\t', game_played.word, '\t'+game_played.status,
                  '  ', game_played.bad_guesses, '\t\t\t', game_played.missed_letters, '\t', game_played.score)
        print('\nFinal Score: ', final_score)

    def check_my_letter(self, game_object, current_guess, letter_frequencies):
        """
        check_my_letter method is triggerred and do the operations when the player wants to flip a single letter.
        and flips the current_guess word data if that letter exists if not increments the missed letters
        :param game_object: current game object
        :param current_guess: current_status of the user guess
        :param letter_frequencies: frequencies of letters used and is used to count score obtained by the player
        :return: current_guess: frequencies of letters used and is used to count score obtained by the player
        """
        letter_read = input('Enter a letter')
        count = 0
        score = 0
        if letter_read in game_object.word and current_guess.find(letter_read) == -1:
            for letter in game_object.word:
                if letter == letter_read:
                    score += letter_frequencies[letter]
                    current_guess = current_guess[:count] + letter_read + current_guess[count+1:]
                count += 1
        else:
            print('Oops!! Not this letter')
            game_object.set_missed_letters()
        if current_guess.find('-') == -1:
            print('You won !!!\n\n')
            game_object.set_status('Success')
            game_object.set_score(score)
        return current_guess

    def tell_me(self, game_object, current_guess, letter_frequencies):
        """
        tell_me method shows the user that he couldn't guess the word and informs the word and starts
        and the user loss 10% of his current score.
        :param game_object: current game object
        :param current_guess: current guess word upto user guessed
        :param letter_frequencies: frequencies of letters used and is used to count score obtained by the player
        :return: current_guess: current_guess string
        """
        game_object.set_status('Gave Up')
        print('You lost this time. The word is ' + game_object.word + '\n\n')
        while current_guess.find('-') != -1:
            index = current_guess.index('-')
            letter_at_index = game_object.word[index]
            current_guess = current_guess[:index] + letter_at_index + current_guess[index + 1:]
            game_object.set_score(-1 * letter_frequencies[letter_at_index])
        return current_guess

    def guess_word(self, game_object, current_guess, letter_frequencies):
        """
        guess_word checks the random word with the user guessed word. If the user guesses correctly then score will be added
        by the sum of the un flipped characters frequency, else bad guess will be incremented
        :param game_object: current game status object
        :param current_guess: current_guess status of the user
        :param letter_frequencies: frequencies of letters used and is used to count score obtained by the player
        :return: current_guess
        """
        word_read = input('Enter the word completely:')
        if word_read == game_object.word:
            print('You won !!!\n\n')
            game_object.set_status('Success')
            while current_guess.find('-') != -1:
                index = current_guess.index('-')
                letter_at_index = game_object.word[index]
                current_guess = current_guess[:index] + letter_at_index + current_guess[index + 1:]
                game_object.set_score(letter_frequencies[letter_at_index])
        else:
            print('You went wrong !!!')
            game_object.set_bad_guesses()
            game_object.set_score(-1 * (10 / 100 * abs(game_object.score)))
        return current_guess


if __name__ == '__main__':
    string_database = stringDatabase.StringDatabase()
    word_list = string_database.get_words()
    word_score = string_database.get_scores_for_words()
    Guess().start_game(word_list, word_score)
